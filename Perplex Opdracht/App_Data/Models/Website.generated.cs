//------------------------------------------------------------------------------
// <auto-generated>
//   This code was generated by a tool.
//
//    Umbraco.ModelsBuilder v8.1.0
//
//   Changes to this file will be lost if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web;
using Umbraco.ModelsBuilder;
using Umbraco.ModelsBuilder.Umbraco;

namespace Umbraco.Web.PublishedModels
{
	/// <summary>Website</summary>
	[PublishedModel("website")]
	public partial class Website : PublishedContentModel, IContentComposition, IHeaderComposition, IPageSettingsComposition
	{
		// helpers
#pragma warning disable 0109 // new is redundant
		[global::System.CodeDom.Compiler.GeneratedCodeAttribute("Umbraco.ModelsBuilder", "8.1.0")]
		public new const string ModelTypeAlias = "website";
		[global::System.CodeDom.Compiler.GeneratedCodeAttribute("Umbraco.ModelsBuilder", "8.1.0")]
		public new const PublishedItemType ModelItemType = PublishedItemType.Content;
		[global::System.CodeDom.Compiler.GeneratedCodeAttribute("Umbraco.ModelsBuilder", "8.1.0")]
		public new static IPublishedContentType GetModelContentType()
			=> PublishedModelUtility.GetModelContentType(ModelItemType, ModelTypeAlias);
		[global::System.CodeDom.Compiler.GeneratedCodeAttribute("Umbraco.ModelsBuilder", "8.1.0")]
		public static IPublishedPropertyType GetModelPropertyType<TValue>(Expression<Func<Website, TValue>> selector)
			=> PublishedModelUtility.GetModelPropertyType(GetModelContentType(), selector);
#pragma warning restore 0109

		// ctor
		public Website(IPublishedContent content)
			: base(content)
		{ }

		// properties

		///<summary>
		/// Items: Select the pages that you want to show in the navigation bar
		///</summary>
		[global::System.CodeDom.Compiler.GeneratedCodeAttribute("Umbraco.ModelsBuilder", "8.1.0")]
		[ImplementPropertyType("navigationItems")]
		public IEnumerable<IPublishedContent> NavigationItems => this.Value<IEnumerable<IPublishedContent>>("navigationItems");

		///<summary>
		/// Page Title Suffix: This text will be added to the end of the page's browser tab title
		///</summary>
		[global::System.CodeDom.Compiler.GeneratedCodeAttribute("Umbraco.ModelsBuilder", "8.1.0")]
		[ImplementPropertyType("pageTitleSuffix")]
		public string PageTitleSuffix => this.Value<string>("pageTitleSuffix");

		///<summary>
		/// Content: The content of this page
		///</summary>
		[global::System.CodeDom.Compiler.GeneratedCodeAttribute("Umbraco.ModelsBuilder", "8.1.0")]
		[ImplementPropertyType("content")]
		public Newtonsoft.Json.Linq.JToken Content => ContentComposition.GetContent(this);

		///<summary>
		/// Header Description: The small description shown in the header of the page
		///</summary>
		[global::System.CodeDom.Compiler.GeneratedCodeAttribute("Umbraco.ModelsBuilder", "8.1.0")]
		[ImplementPropertyType("headerDescription")]
		public string HeaderDescription => HeaderComposition.GetHeaderDescription(this);

		///<summary>
		/// Header Title: The title shown in the header of the page
		///</summary>
		[global::System.CodeDom.Compiler.GeneratedCodeAttribute("Umbraco.ModelsBuilder", "8.1.0")]
		[ImplementPropertyType("headerTitle")]
		public string HeaderTitle => HeaderComposition.GetHeaderTitle(this);

		///<summary>
		/// Do not append Page Title suffix: If this option is enabled, the website's Page Title suffix (found on the Home Page) will not be shown in the page's browser tab title
		///</summary>
		[global::System.CodeDom.Compiler.GeneratedCodeAttribute("Umbraco.ModelsBuilder", "8.1.0")]
		[ImplementPropertyType("doNotAppendPageTitleSuffix")]
		public bool DoNotAppendPageTitleSuffix => PageSettingsComposition.GetDoNotAppendPageTitleSuffix(this);

		///<summary>
		/// Page Title: The title of this page
		///</summary>
		[global::System.CodeDom.Compiler.GeneratedCodeAttribute("Umbraco.ModelsBuilder", "8.1.0")]
		[ImplementPropertyType("pageTitle")]
		public string PageTitle => PageSettingsComposition.GetPageTitle(this);
	}
}
