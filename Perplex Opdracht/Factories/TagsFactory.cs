﻿using Perplex_Opdracht.Models;
using Umbraco.Core.Models.PublishedContent;

namespace Perplex_Opdracht.Factories
{
    public class TagsFactory
    {
        public static Tag CreateTag(IPublishedContent content)
        {
            return new Tag
            {
                Id = content.Id,
                Name = content.Name
            };
        }
    }
}