﻿using Perplex_Opdracht.Models;
using System.Linq;
using Umbraco.Core;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web.PublishedModels;

namespace Perplex_Opdracht.Factories
{
    public class NewsFactory
    {
        public static NewsArticle CreateArticle(NewsPage content)
        {
            var tags = content.RelatedTags.Select(x => x.Name).ToList();

            return new NewsArticle
            {
                Id = content.Id,
                Url = content.Url,
                Title = content.HeaderTitle,
                Description = content.HeaderDescription.Truncate(256),
                Image = content.HeaderImage.Url,
                Date = content.Date.ToString("dd-MM-yyyy HH:mm"),
                Tags = tags
            };
        }
    }
}