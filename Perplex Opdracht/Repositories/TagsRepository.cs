﻿using Perplex_Opdracht.Factories;
using Perplex_Opdracht.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web;

namespace Perplex_Opdracht.Repositories
{
    public class TagsRepository
    {
        public static List<Tag> GetTags(IPublishedContent currentPage)
        {
            return currentPage.AncestorOrSelf("website").DescendantOfType("tagsFolder").Children
                .Select(x => TagsFactory.CreateTag(x))
                .ToList();
        }
    }
}