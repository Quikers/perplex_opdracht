﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Perplex_Opdracht.Controllers
{
    public class NewsPageController : BaseController
    {
        public ActionResult Index()
        {
            return View(CurrentPage);
        }
    }
}