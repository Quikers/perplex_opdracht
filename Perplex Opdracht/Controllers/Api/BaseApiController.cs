﻿using Perplex_Opdracht.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Web.WebApi;

namespace Perplex_Opdracht.Controllers.Api
{
    public class BaseApiController : UmbracoApiController
    {
        public BaseApiController() : base()
        {
            if (UmbracoConstants.Instance == null)
            {
                UmbracoConstants.Instance = new UmbracoConstants();
            }
        }
    }
}