﻿using Newtonsoft.Json;
using Perplex_Opdracht.Constants;
using Perplex_Opdracht.Factories;
using Perplex_Opdracht.Models;
using Perplex_Opdracht.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Results;
using Umbraco.Web;
using Umbraco.Web.PublishedModels;
using Umbraco.Web.WebApi;

namespace Perplex_Opdracht.Controllers.Api
{
    public class NewsApiController : BaseApiController
    {

        [HttpGet]
        public JsonResult<PaginatedResult<List<NewsArticle>>> GetArticles(int pageNumber, int pageSize, int filter = -1)
        {
            // Get all news pages
            var articles = UmbracoConstants.Website.DescendantOfType("newsOverviewPage").Children.Cast<NewsPage>();


            // Check if a filter is active
            if (filter > -1)
            {
                // Get only the filtered result
                articles = articles.Where(x => x.RelatedTags.Any(y => y.Id == filter));
            }

            // Take what you need for pagination and parse the NewsPages to NewsArticles
            var filteredArticles = articles.Skip(pageSize * pageNumber).Take(pageSize)
                .Select(x => NewsFactory.CreateArticle(x))
                .ToList();

            // Get the total amount of pages available for this setting (with filter)
            var pagesAvailable = (int)Math.Ceiling((float)articles.Count() / (float)pageSize);

            // Create the result with info about the pagination
            var result = new PaginatedResult<List<NewsArticle>>
            {
                PagesAvailable = pagesAvailable,
                PageNumber = pageNumber,

                Data = filteredArticles
            };

            // Parse the C# object to a JavaScript object
            return Json(result);
        }
    }
}