﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using Umbraco.Web.PublishedModels;

namespace Perplex_Opdracht.Controllers
{
    public class NewsOverviewPageController : BaseController
    {
        public ActionResult Index()
        {
            return View(CurrentPage);
        }
    }
}