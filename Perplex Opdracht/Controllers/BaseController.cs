﻿using Perplex_Opdracht.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Web.Mvc;

namespace Perplex_Opdracht.Controllers
{
    public class BaseController : RenderMvcController
    {
        public BaseController() : base()
        {
            UmbracoConstants.Instance = new UmbracoConstants();
        }
    }
}