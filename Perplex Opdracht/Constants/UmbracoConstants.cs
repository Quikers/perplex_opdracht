﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web;
using Umbraco.Web.PublishedModels;

namespace Perplex_Opdracht.Constants
{
    public class UmbracoConstants
    {
        private IPublishedContent __currentPage;
        private IPublishedContent _currentPage {
            get {
                if (__currentPage == null)
                {
                    __currentPage = Umbraco.Web.Composing.Current.UmbracoHelper.AssignedContentItem;
                }

                return __currentPage;
            }
        }
        private Website __website;
        private Website _website {
            get {
                if (__website == null)
                {
                    __website = _currentPage.AncestorOrSelf("website") as Website;
                }

                return __website;
            }
        }



        public static UmbracoConstants Instance;

        public static IPublishedContent CurrentPage => Instance?._currentPage;
        public static Website Website => Instance?._website;
    }
}