﻿
// A repository for retrieving all articles with the current settings
var newsApiRepository = {
    getArticles: function (pageNumber, pageSize, filter) {
        return $.getJSON("/Umbraco/api/newsapi/getarticles?pageNumber=" + pageNumber + "&pageSize=" + pageSize + "&filter=" + filter);
    }
};

// This object handles the article retrieving, saving (no duplicate gets), pagination, clearing and filling the frontend with the retrieved articles.
var newsApiHandler = {
    $newsContainer: $("#articleContainer"),
    $pageInfoContainer: $("#paginationContainer .page-info"),
    $pagesContainer: $("#paginationContainer .pages"),

    pageSize: 6,
    pagesAvailable: 0,
    currentPage: 0,
    viewBuffer: [],
    pages: [],

    activeFilter: -1,

    // Get the requested page from the API
    retrievePage: function (pageNumber, pageSize, filter) {
        return newsApiRepository.getArticles(pageNumber, pageSize, filter).done(function (data) {
            newsApiHandler.pagesAvailable = data.PagesAvailable;

            var newsArticles = data.Data;

            if (!newsApiHandler.pages[pageNumber]) {
                newsApiHandler.pages[pageNumber] = [];
            }

            for (var i = 0; i < newsArticles.length; i++) {
                newsApiHandler.pages[pageNumber].push(newsArticles[i]);
            }
        });
    },

    // Refresh the frontend with the saved active page
    refreshLayout: function () {
        this.$newsContainer.empty();

        var rows = [];
        for (var rowIndex = 0; rowIndex < this.pages[this.currentPage].length; rowIndex += 3) {

            var rowArticles = [];

            var maxRowIndex = this.pages[this.currentPage].length - rowIndex >= 3 ? 3 : this.pages[this.currentPage].length - rowIndex;

            for (var articleIndex = rowIndex; articleIndex < rowIndex + maxRowIndex; articleIndex++) {
                var article = this.pages[this.currentPage][articleIndex];
                var articleTemplate = `
                    <div class="article col-md-4">
                        <a href="${article.Url}">
                            <img class="img-responsive" src="${article.Image}">
                            <small class="article-date">${article.Date}</small><small class="article-tags">${article.Tags.join(", ")}</small>
                            <h4>${article.Title}</h4>
                            <p>${article.Description}</p>
                        </a>
                    </div>
                `;

                rowArticles.push(articleTemplate);
            }

            var rowTemplate = `
                <div class="row">
                    ${rowArticles[0]}`
                    + (rowArticles.length > 1 ? `${rowArticles[1]}` : ``)
                    + (rowArticles.length > 2 ? `${rowArticles[2]}` : ``)
                + `</div>
            `;

            rows.push(rowTemplate);
        }

        this.$newsContainer.append(rows);
    },

    // Clears the saved buffers
    clearBuffer: function () {
        this.viewBuffer = [];
        this.pages = [];
    },

    // Sets the buffer and automatically gets a missing page from the API
    setViewBuffer: function (pageNumber) {

        if (this.pages[pageNumber]) {
            this.viewBuffer = this.pages[pageNumber];
            this.currentPage = pageNumber;

            newsApiHandler.updatePagination(newsApiHandler.pagesAvailable, pageNumber);

            return;
        }

        return this.retrievePage(pageNumber, this.pageSize, this.activeFilter).then(function () {
            newsApiHandler.viewBuffer = newsApiHandler.pages[pageNumber];
            newsApiHandler.currentPage = pageNumber;

            newsApiHandler.updatePagination(newsApiHandler.pagesAvailable, pageNumber);
        });
    },

    // This initializes the retrieval of a page, if the page has already been downloaded from the API then it will just refresh the layout
    getPage: function (pageNumber) {
        var result = this.setViewBuffer(pageNumber);

        if (!result) {
            newsApiHandler.refreshLayout();
        }
        else {
            result.then(function () {
                newsApiHandler.refreshLayout();
            });
        }
    },

    // Refreshes the pagination at the bottom of the list of articles
    updatePagination: function (pagesAvailable, newPageNumber) {
        this.$pagesContainer.empty();
        this.$pageInfoContainer.empty();

        var pageInfoTemplate = `Page ${newPageNumber + 1} out of ${pagesAvailable}`;
        this.$pageInfoContainer.append(pageInfoTemplate);

        for (var i = 0; i < pagesAvailable; i++) {
            var pageTemplate = `<a class="page` + (i == newPageNumber ? " active" : "") + `" data-index="${i}" href="#">${i + 1}</a>`;

            this.$pagesContainer.append(pageTemplate);
        }

        // Give the newly created page links an onclick event 
        $(".page").on("click", function () {
            var pageNumber = parseInt($(this).attr("data-index"));

            newsApiHandler.getPage(pageNumber);
        });
    },

    // Applies a specific (or -1 for none) filter
    applyFilter: function (filter) {
        this.clearBuffer();

        if (!filter || filter < 0) {
            filter = null;
        }

        this.activeFilter = filter;

        this.getPage(0);
    },
};


$(document).ready(function () {

    // Initialize the filter dropdown box
    $("#filterDropdown").on("change", function () {
        newsApiHandler.applyFilter(parseInt($(this).val()))
    });

    // Initialize the first page of the news articles
    newsApiHandler.getPage(0);

});