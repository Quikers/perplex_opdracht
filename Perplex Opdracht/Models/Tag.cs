﻿namespace Perplex_Opdracht.Models
{
    public class Tag
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}