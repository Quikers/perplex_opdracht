﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Perplex_Opdracht.Models
{
    public class PaginatedResult<T>
    {
        public int PagesAvailable { get; set; }
        public int PageNumber { get; set; }

        public T Data { get; set; }
    }
}